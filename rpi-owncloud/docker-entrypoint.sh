#!/bin/bash

set -e

echo "Using the followin' environment setup ..."
echo "env.OWNCLOUD_DIFFIE_HELLMAN is $OWNCLOUD_DIFFIE_HELLMAN"
echo "env.OWNCLOUD_VERSION is $OWNCLOUD_VERSION"
echo "env.OWNCLOUD_BASE_DIR is $OWNCLOUD_BASE_DIR"
echo "env.OWNCLOUD_CERT_DIR is $OWNCLOUD_CERT_DIR"
echo "env.OWNCLOUD_DATA_DIR is $OWNCLOUD_DATA_DIR"
echo "env.OWNCLOUD_CONFIG_DIR is $OWNCLOUD_CONFIG_DIR"
echo "env.OWNCLOUD_SERVERNAME is $OWNCLOUD_SERVERNAME"

if [ -z "$OWNCLOUD_SERVERNAME" ]; then
    echo >&2 'error: you have to provide a server-name'
    echo >&2 '  Did you forget to add -e OWNCLOUD_SERVERNAME=... ?'
    exit 1
fi

if [ ! -d "$OWNCLOUD_DATA_DIR" ]; then
    echo >&2 "error: there is no data-dir available!!"
    echo >&2 "   Are you sure you've mounted a volume from the data container??"
    exit 2
fi

echo "Updating the servers hostname (to $OWNCLOUD_SERVERNAME)"
sudo sed -i "s/server_name localhost/server_name $OWNCLOUD_SERVERNAME/g" /etc/nginx/sites-available/default

if [ ! -f "$OWNCLOUD_CONFIG_DIR/configuration.done" ]; then
    echo "Creating configuration in $OWNCLOUD_CONFIG_DIR"

    sudo cp /tmp/autoconfig.tmpl $OWNCLOUD_CONFIG_DIR/autoconfig.php
    sudo sed -i "s@\"directory\".*,\$@\"directory\" => \"$OWNCLOUD_DATA_DIR\",@g" $OWNCLOUD_CONFIG_DIR/autoconfig.php

    echo "Setting up db credentials ($OWNCLOUD_DB_USER/********)"
    sudo sed -i "s@\"dbuser\".*,\$@\"dbuser\" => \"$OWNCLOUD_DB_USER\",@g" $OWNCLOUD_CONFIG_DIR/autoconfig.php
    sudo sed -i "s@\"dbpass\".*,\$@\"dbpass\" => \"$OWNCLOUD_DB_PASSWORD\",@g" $OWNCLOUD_CONFIG_DIR/autoconfig.php
    sudo touch $OWNCLOUD_CONFIG_DIR/configuration.done
fi

# prepare the stage for letsencrypt requests
if [ -d "$OWNCLOUD_DATA_DIR" ] && [ ! -d "$OWNCLOUD_DATA_DIR/letsencrypt" ]; then
    echo "Creating the directory where letsencrypt will store its challanges ..."
    sudo mkdir -p $OWNCLOUD_DATA_DIR/letsencrypt
fi

if [ ! -f "$OWNCLOUD_CERT_DIR/owncloud.pem" ]; then
    echo "Generating self-signed certificates ..."
    echo "Notice: You can overwrite these 'untrusted' certificates with a trausted letsencrypt"
    echo "        certificate - just use the schoeffm/rpi-dehydrated image!"

    sudo mkdir -p $OWNCLOUD_CERT_DIR
    sudo openssl genrsa -out $OWNCLOUD_CERT_DIR/owncloud.key 4096 
    sudo openssl req -new -sha256 -batch -subj "/CN=$OWNCLOUD_SERVERNAME" -key $OWNCLOUD_CERT_DIR/owncloud.key -out $OWNCLOUD_CERT_DIR/owncloud.csr 
    sudo openssl x509 -req -sha256 -days 3650 -in $OWNCLOUD_CERT_DIR/owncloud.csr -signkey $OWNCLOUD_CERT_DIR/owncloud.key -out $OWNCLOUD_CERT_DIR/owncloud.pem
fi


# in order to activate Diffie-Hellman for TLS
if [ ! -f "$OWNCLOUD_CERT_DIR/dhparam.pem" ] && [ "$OWNCLOUD_DIFFIE_HELLMAN" == "on" ]; then
    echo "env.OWNCLOUD_DIFFIE_HELLMAN is $OWNCLOUD_DIFFIE_HELLMAN"
    echo "Will generate a 2048-bit long Diffie-Hellman Params File (take a 20 minute break)"    

    # on rpi this comman will take a huge period of time ... so be patient
    sudo openssl dhparam -out $OWNCLOUD_CERT_DIR/dhparam.pem 2048
    sudo sed -i "s@# ssl_dhparam@ssl_dhparam $OWNCLOUD_CERT_DIR/dhparam.pem;@g" /etc/nginx/sites-available/default
fi

exec "$@"
